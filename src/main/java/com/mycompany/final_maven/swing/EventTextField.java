package com.mycompany.final_maven.swing;

public interface EventTextField {

    public void onPressed(EventCallBack call);

    public void onCancel();
}
