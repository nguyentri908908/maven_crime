package com.mycompany.final_maven.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;

@XmlRootElement(
        name = "RegisterToVisits"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class RegisterToVisitXML {
    private List<RegisterToVisit> RegisterToVisit;

    public RegisterToVisitXML() {
    }

    public List<RegisterToVisit> getRegisterToVisit() {
        return this.RegisterToVisit;
    }

    public void setRegisterToVisit(List<RegisterToVisit> RegisterToVisit) {
        this.RegisterToVisit = RegisterToVisit;
    }
}