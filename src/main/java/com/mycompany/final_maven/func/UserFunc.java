/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.final_maven.func;

/**
 *
 * @author Nguyen Duc Tri
 */

import com.mycompany.final_maven.entity.User;

public class UserFunc {
    public boolean checkUser(User user) {
        if (user != null) {
            if ("admin".equals(user.getUserName())
                    && "admin".equals(user.getPassword())) {
                return true;
            }
        }
        return false;
    }
}