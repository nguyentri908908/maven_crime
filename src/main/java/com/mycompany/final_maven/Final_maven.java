/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.final_maven;

/**
 *
 * @author Nguyen Duc Tri
 */
import com.mycompany.final_maven.View.DashBoardView;
import com.mycompany.final_maven.View.LoginView;
import com.mycompany.final_maven.controller.DashBoardController;
import com.mycompany.final_maven.controller.LoginController;
import java.io.*;
import java.awt.EventQueue;
public class Final_maven {

     public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {

                LoginView view = new LoginView();
                LoginController controller = new LoginController(view);
                // hiển thị màn hình login
                controller.showLoginView();
//                DashBoardView view = new DashBoardView();
//                DashBoardController controller = new DashBoardController(view);
//                controller.showDashView();
            }
        });
    }
}
