/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.final_maven.controller;


import com.mycompany.final_maven.View.DashBoardView;
import com.mycompany.final_maven.View.LoginView;
import com.mycompany.final_maven.entity.User;
import com.mycompany.final_maven.func.UserFunc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class LoginController {
    private UserFunc userDao;
    private LoginView loginView;
    //private StudentView studentView;

    public LoginController(LoginView view) {
        this.loginView = view;
        this.userDao = new UserFunc();
        view.addLoginListener(new LoginListener());
    }

    public void showLoginView() {
        loginView.setVisible(true);
    }

    /**
     * Lớp LoginListener chứa cài đặt cho sự kiện click button "Login"
     *
     * @author viettuts.vn
     */
    class LoginListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            User user = loginView.getUser();
            if (userDao.checkUser(user)) {
                 //nếu đăng nhập thành công, mở dashboard
                DashBoardView view = new DashBoardView();
                DashBoardController dashController = new DashBoardController(view);
                dashController.showDashView();
                loginView.setVisible(false);
            } else {
                loginView.showMessage("username hoặc password không đúng.");
            }
//            studentView = new StudentView();
//            StudentController studentController = new StudentController(studentView);
//            studentController.showStudentView();
//            loginView.setVisible(false);
        }
    }
}
