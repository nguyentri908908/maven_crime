package com.mycompany.final_maven.controller;

import com.mycompany.final_maven.entity.Crime;
import com.mycompany.final_maven.form.Form3;
import com.mycompany.final_maven.func.CimeFunc;

import java.util.List;

public class Form3Controller {
    private CimeFunc crimeFunc;
    private Form3 view;
    public Form3Controller(Form3 view){
        this.view=view;
        crimeFunc = new CimeFunc();
    }
    public void showForm3(){
        List<Crime> crimeList = crimeFunc.getListCrimes();
        view.showListCrimes(crimeList);
    }
}
