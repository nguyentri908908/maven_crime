package com.mycompany.final_maven.event;

public interface EventMenuSelected {

    public void selected(int index);
}
